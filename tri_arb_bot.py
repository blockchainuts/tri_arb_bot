import msvcrt
from datetime import datetime
from math import *
from time import *

from requests import *

import utils
from tri_arb_bot_api import *

log_file_name = f'[TRIARBI]' + datetime.now().strftime('[%Y-%m-%d][%Hh%Mm%Ss]') + '.txt'
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s   %(message)s',
                    datefmt='%d/%m/%y  %H:%M:%S',
                    handlers=[logging.FileHandler(log_file_name), logging.StreamHandler()])

class KTriArbBot:
    name = 'KRYPTONO'

    def __init__(self):
        self.coins = ['BTC', 'ETH', 'USDT', 'KNOW', 'GTO', 'XEM', 'TRX', 'XPX']
        self.pairs = ['ETH_BTC', 'BTC_USDT', 'ETH_USDT', 'BTC_USDT', 'KNOW_BTC', 'KNOW_ETH', 'KNOW_USDT', 'GTO_ETH',
                      'GTO_BTC', 'GTO_KNOW', 'GTO_USDT', 'TRX_ETH', 'TRX_BTC', 'XPX_ETH', 'XPX_BTC', 'XEM_ETH',
                      'XEM_BTC']

        self.prev_balances = {}
        self.curr_balances = {}
        self.prev_total_balances = {}
        self.curr_total_balances = {}
        self.total_profit = {}
        self.base_asset_precision = {}
        self.quote_asset_precision = {}
        self.min_total_qty = {}
        self.min_qty = {}

        self.get_exchange_info()

        self.bot = Kryptono(config.KRYPTONO_API_KEY, config.KRYPTONO_API_SECRET)
        
        self.number_of_txs = 0

        for coin in self.coins:
            self.prev_balances[coin] = float(self.bot.getBalanceAvailable(coin))
            self.prev_total_balances[coin] = float(self.bot.getBalanceTotal(coin))
            self.curr_balances[coin] = self.prev_balances[coin]
            self.curr_total_balances[coin] = self.prev_total_balances[coin]
            self.total_profit[coin] = 0

        logging.info('============ INITIAL BALANCES ============')
        for coin in self.coins:
            logging.info(f'\t{coin}: {self.curr_balances[coin]}')
        logging.info('==========================================')

    def get_exchange_info(self):
        exchange_info = requests.get(config.KRYPTONO_URL_API_GET_EXCHANGE_INFO).json()

        for symbol_info in exchange_info['symbols']:
            self.base_asset_precision[symbol_info['symbol']] = symbol_info['amount_limit_decimal']
            self.quote_asset_precision[symbol_info['symbol']] = symbol_info['price_limit_decimal']

        for base_currency in exchange_info['base_currencies']:
            self.min_total_qty[base_currency['currency_code']] = float(base_currency['minimum_total_order']) * 1.1

        for coin_info in exchange_info['coins']:
            self.min_qty[coin_info['currency_code']] = float(coin_info['minimum_order_amount']) * 1.1

    def get_bid_market_order(self, pair):
        try:
            order_book = requests.get(config.KRYPTONO_URL_ENGINES_GET_MARKET_ORDER + pair).json()[pair]
        except:
            logging.info(f'Cannot get order on pair {pair}')
            return False

        if 'bids' not in order_book:
            return False

        if not order_book['bids']:
            return False

        return { 'bid_price': utils.normalize(float(order_book['bids']['price']) / 10**8, self.quote_asset_precision[pair]), 'bid_qty' : float(order_book['bids']['amount']) / 10**8 }
    
    def get_ask_market_order(self, pair):
        try:
            order_book = requests.get(config.KRYPTONO_URL_ENGINES_GET_MARKET_ORDER + pair).json()[pair]
        except:
            logging.info(f'Cannot get ask orders on pair {pair}')
            return False

        if 'asks' not in order_book:
            return False

        if not order_book['asks']:
            return False

        return { 'ask_price' : utils.normalize(float(order_book['asks']['price']) / 10**8, self.quote_asset_precision[pair]), 'ask_qty' : float(order_book['asks']['amount']) / 10**8 }

    def place_order(self, side, symbol, price, qty):
        t = 1
        while True:
            order = self.bot.createOrder(side, symbol, price, qty)

            if 'order_id' in order:
                break

            if 'error' in order:
                utils.send_telegram_message(f"Cannot place {side} order on {symbol}. Error message: {order['error_description']}")
                sleep(t)
                t = (t + 1) % config.MAX_WAIT_TIME
                # return { 'status': 'failure', 'error': order['error_description'] }
        return {'status': 'success', 'orderId': order['order_id']}


    def cancel_order(self, orderId):
        while True:
            cancel_status = self.bot.cancelOrder(self.symbol, orderId)
            logging.info(f'{self.name} cancelled order: {cancel_status}\n')
            if cancel_status is not None and 'order_id' in cancel_status:
                logging.info('\t... Successfully cancelled order')
                break
            msg = f'Arbitrage bot {BotFather.first_symbol}/{BotFather.second_symbol} on {self.name} cannot cancel order.'
            utils.send_telegram_message(msg)
            logging.info(msg)
            time.sleep(3)
        return True

    def get_order_detail(self, order_id):
        order = self.bot.getOrderDetail(order_id)
        return {'status': order['status'], 'symbol': order['order_symbol'], 'orderSide': order['order_side'], 'originQty': order['order_size'], 'executedQty': order['executed'], 'price': order['order_price'], 'total': order['total']}

    def update_balance(self):
        for coin in self.coins:
            self.prev_balances[coin] = self.curr_balances[coin]
            self.prev_total_balances[coin] = self.curr_total_balances[coin]
            while True:
                self.curr_balances[coin] = float(self.bot.getBalanceAvailable(coin))
                self.curr_total_balances[coin] = float(self.bot.getBalanceTotal(coin))
                if self.curr_balances[coin] != 0 or self.curr_total_balances[coin] != 0:
                    break
                time.sleep(config.WAIT_FOR_BALANCE_UPDATE)

    def handle_errors(self, order, symbol):
        if order['status'] == 'failure':
            msg = f"An error occurred on {symbol}. Error message: {order['error']}"
            utils.send_telegram_message(msg)
            return False
        else:
            logging.info(f"\t\tSuccessfully placed order! OrderID: {order['orderId']}")
            return True

    def balance_stat(self):
        logging.info('Balance statistics')

        headers = ['Asset', 'Before', 'After', 'Diff.']

        rows = []
        for coin in self.coins:
            rows.append([coin, self.prev_total_balances[coin], self.curr_total_balances[coin],
                     self.curr_total_balances[coin] - self.prev_total_balances[coin]])

        logging.info('_' * 80)
        logging.info('{:>8} {:>20} {:>20} {:>20}'.format(*headers))
        logging.info('_' * 80)
        for row in rows:
            logging.info('{:>8} {:20.8f} {:20.8f} {:20.8f}'.format(*row))
        logging.info('_' * 80)

    def bid_bid_ask(self, X, Y, Z, X_Y, Y_Z, X_Z, X_Y_bid, Y_Z_bid, X_Z_ask):
        X_Z_implicit_bid_price = utils.normalize(X_Y_bid['bid_price'] * Y_Z_bid['bid_price'],
                                                 self.quote_asset_precision[X_Z])
        gap = utils.normalize(X_Z_implicit_bid_price - X_Z_ask['ask_price'], self.quote_asset_precision[X_Z])

        if gap <= 0:
            return True

        logging.info(f'============ TRIANGULAR ARBITRAGE OPPORTUNITY DETECTED ({X} - {Y} - {Z}) ============')
        logging.info(f"Someone is buying {X_Y_bid['bid_qty']} {X} at price {X_Y_bid['bid_price']} {X}/{Y}")
        logging.info(f"Someone is buying {Y_Z_bid['bid_qty']} {Y} at price {Y_Z_bid['bid_price']} {Y}/{Z}")
        logging.info(f"Someone is selling {X_Z_ask['ask_qty']} {X} at price {X_Z_ask['ask_price']} {X}/{Z}")
        logging.info(f'Pricing discrepancy: {gap} {X}/{Z}')

        # ask_qty_X = min(X_Y_bid['bid_qty'], X_Z_ask['ask_qty'])
        ask_qty_X = self.min_qty[X]
        ask_qty_X = max(ask_qty_X, self.min_total_qty[Y] / X_Y_bid['bid_price'])
        ask_qty_X = max(ask_qty_X, self.min_total_qty[Z] / X_Z_ask['ask_price'])
        ask_qty_X = utils.normalize(ask_qty_X, self.base_asset_precision[X_Y])
        gain_qty_Y = float(format(ask_qty_X * X_Y_bid['bid_price'], '.8f'))
        ask_qty_Y = utils.normalize(gain_qty_Y, self.base_asset_precision[Y_Z])
        gain_qty_Z = float(format(ask_qty_Y * Y_Z_bid['bid_price'], '.8f'))
        gain_qty_X = utils.normalize(gain_qty_Z / X_Z_ask['ask_price'], self.base_asset_precision[X_Z])
        pay_qty_Z = float(format(gain_qty_X * X_Z_ask['ask_price'], '.8f'))

        profit_X = float(format(gain_qty_X - ask_qty_X, '.8f'))
        profit_Z = float(format(gain_qty_Z - pay_qty_Z, '.8f'))

        if profit_X <= 0 or profit_Z <= 0:
            logging.info('Profit is zero. Wait for another opportunity...')
            return True

        self.update_balance()

        if self.curr_balances[X] < ask_qty_X:
            logging.info(f'Cannot place orders since bot does not have enough {X}')
            return True

        if self.curr_balances[Y] < gain_qty_Y:
            print(self.curr_balances[Y])
            logging.info(f'Cannot place orders since bot does not have enough {Y}')
            return True

        logging.info(f'The bot will proceed as follows:')
        logging.info(
            f"1. Sell {ask_qty_X} {X} at price {X_Y_bid['bid_price']} {X}/{Y}, gain {gain_qty_Y} {Y} and pay {ask_qty_X} {X}")
        sell_order_X_Y = self.place_order('sell', X_Y, X_Y_bid['bid_price'], ask_qty_X)
        if not self.handle_errors(sell_order_X_Y, X_Y):
            return False

        logging.info(
            f"2. Sell {ask_qty_Y} {Y} at price {Y_Z_bid['bid_price']} {Y}/{Z}, gain {gain_qty_Z} {Z} and pay {ask_qty_Y} {Y}")
        sell_order_Y_Z = self.place_order('sell', Y_Z, Y_Z_bid['bid_price'], ask_qty_Y)
        if not self.handle_errors(sell_order_Y_Z, Y_Z):
            return False

        logging.info(
            f"3. Buy {gain_qty_X} {X} at price {X_Z_ask['ask_price']} {X}/{Z} with {gain_qty_Z} {Z}, gain {gain_qty_X} {X} and pay {pay_qty_Z} {Z}")
        buy_order_X_Z = self.place_order('buy', X_Z, X_Z_ask['ask_price'], gain_qty_X)
        if not self.handle_errors(buy_order_X_Z, X_Z):
            return False

        X_Y_bid_detail = self.get_order_detail(sell_order_X_Y['orderId'])
        Y_Z_bid_detail = self.get_order_detail(sell_order_Y_Z['orderId'])
        X_Z_ask_detail = self.get_order_detail(buy_order_X_Z['orderId'])

        # logging.info('Waiting for orders to be filled...')
        # while True:
        #     X_Y_bid_detail = self.get_order_detail(sell_order_X_Y['orderId'])
        #     Y_Z_bid_detail = self.get_order_detail(sell_order_Y_Z['orderId'])
        #     X_Z_ask_detail = self.get_order_detail(buy_order_X_Z['orderId'])
        #     if X_Y_bid_detail['status'] == 'filled' and Y_Z_bid_detail['status'] == 'filled' and X_Z_ask_detail[
        #         'status'] == 'filled':
        #         break
        #     time.sleep(config.WAIT_FOR_FILLED)

        self.total_profit[X] += profit_X
        self.total_profit[Z] += profit_Z
        self.number_of_txs += 1

        logging.info(f"1. Sell order detail ({X}/{Y}): {X_Y_bid_detail}")
        logging.info(f"2. Sell order detail ({Y}/{Z}): {Y_Z_bid_detail}")
        logging.info(f"3. Buy order detail ({X}/{Z}): {X_Z_ask_detail}")
        logging.info(f'Profit of this round: {profit_X} {X} and {profit_Z} {Z}')
        logging.info(f'Total profit: {self.total_profit[X]} {X} and {self.total_profit[Z]} {Z}')
        logging.info(f'Number of transactions: {self.number_of_txs}')
        self.update_balance()
        self.balance_stat()
        logging.info('======================================================================================')

        return True

    def ask_ask_bid(self, X, Y, Z, X_Y, Y_Z, X_Z, X_Y_ask, Y_Z_ask, X_Z_bid):
        X_Z_implicit_ask_price = utils.normalize(X_Y_ask['ask_price'] * Y_Z_ask['ask_price'], self.quote_asset_precision[X_Z])
        gap = utils.normalize(X_Z_bid['bid_price'] - X_Z_implicit_ask_price, self.quote_asset_precision[X_Z])

        if gap <= 0:
            return True

        logging.info(f'============ TRIANGULAR ARBITRAGE OPPORTUNITY DETECTED ({X} - {Y} - {Z}) ============')
        logging.info(f"Someone is selling {X_Y_ask['ask_qty']} {X} at price {X_Y_ask['ask_price']} {X}/{Y}")
        logging.info(f"Someone is selling {Y_Z_ask['ask_qty']} {Y} at price {Y_Z_ask['ask_price']} {Y}/{Z}")
        logging.info(f"Someone is buying {X_Z_bid['bid_qty']} {X} at price {X_Z_bid['bid_price']} {X}/{Z}")
        logging.info(f'Pricing discrepancy: {gap} {X}/{Z}')

        # bid_qty_X = min(X_Y_ask['ask_qty'], X_Z_bid['bid_qty'])
        bid_qty_X = self.min_qty[X]
        bid_qty_X = max(bid_qty_X, self.min_total_qty[Z] / X_Z_bid['bid_price'])
        bid_qty_X = max(bid_qty_X, self.min_total_qty[Y] / X_Y_ask['ask_price'])
        bid_qty_X = utils.normalize(bid_qty_X, self.base_asset_precision[X_Y])

        pay_Y = float(format(bid_qty_X * X_Y_ask['ask_price'], '.8f'))
        gain_Z = float(format(bid_qty_X * X_Z_bid['bid_price'], '.8f'))

        bid_qty_Y = utils.normalize(gain_Z / Y_Z_ask['ask_price'], self.base_asset_precision[Y_Z])
        pay_Z = float(format(bid_qty_Y * Y_Z_ask['ask_price'], '.8f'))

        profit_Y = float(format(bid_qty_Y - pay_Y, '.8f'))
        profit_Z = float(format(gain_Z - pay_Z, '.8f'))

        if profit_Y <= 0 or profit_Z <= 0:
            logging.info('Profit is zero. Wait for another opportunity...')
            return True

        self.update_balance()

        if self.curr_balances[X] < bid_qty_X:
            logging.info(f'Cannot place orders since bot does not have enough {X}')
            return True

        if self.curr_balances[Y] < pay_Y:
            logging.info(f'Cannot place orders since bot does not have enough {Y}')
            return True

        if self.curr_balances[Z] < gain_Z:
            logging.info(f'Cannot place orders since bot does not have enough {Z}')
            return True

        logging.info(f'The bot will proceed as follows:')
        logging.info(f"1. Buy {bid_qty_X} {X} at price {X_Y_ask['ask_price']} {X}/{Y}, gain {bid_qty_X} {X} and pay {pay_Y} {Y}")
        buy_order_X_Y = self.place_order('buy', X_Y, X_Y_ask['ask_price'], bid_qty_X)
        if not self.handle_errors(buy_order_X_Y, X_Y):
            return False

        logging.info(f"2. Sell {bid_qty_X} {X} at price {X_Z_bid['bid_price']} {X}/{Z}, gain {gain_Z} {Z} and pay {bid_qty_X} {X}")
        sell_order_X_Z = self.place_order('sell', X_Z, X_Z_bid['bid_price'], bid_qty_X)
        if not self.handle_errors(sell_order_X_Z, X_Z):
            return False

        logging.info(f"3. Buy {bid_qty_Y} {Y} at price {Y_Z_ask['ask_price']} {Y}/{Z} with {gain_Z} {Z}, gain {bid_qty_Y} {Y} and pay {pay_Z} {Z}")
        buy_order_Y_Z = self.place_order('buy', Y_Z, Y_Z_ask['ask_price'], bid_qty_Y)
        if not self.handle_errors(buy_order_Y_Z, Y_Z):
            return False

        X_Y_bid_detail = self.get_order_detail(buy_order_X_Y['orderId'])
        X_Z_ask_detail = self.get_order_detail(sell_order_X_Z['orderId'])
        Y_Z_bid_detail = self.get_order_detail(buy_order_Y_Z['orderId'])

        # logging.info('Waiting for orders to be filled...')
        # while True:
        #     X_Y_bid_detail = self.get_order_detail(buy_order_X_Y['orderId'])
        #     X_Z_ask_detail = self.get_order_detail(sell_order_X_Z['orderId'])
        #     Y_Z_bid_detail = self.get_order_detail(buy_order_Y_Z['orderId'])
        #     if X_Y_bid_detail['status'] == 'filled' and X_Z_ask_detail['status'] == 'filled' and Y_Z_bid_detail['status'] == 'filled':
        #         break
        #     time.sleep(config.WAIT_FOR_FILLED)

        self.total_profit[Y] += profit_Y
        self.total_profit[Z] += profit_Z
        self.number_of_txs += 1

        logging.info(f'1. Buy order detail ({X}/{Y}): {X_Y_bid_detail}')
        logging.info(f'2. Sell order detail ({X}/{Z}): {X_Z_ask_detail}')
        logging.info(f'3. Buy order detail ({Y}/{Z}): {Y_Z_bid_detail}')
        logging.info(f'Profit of this round: {profit_Y} {Y} and {profit_Z} {Z}')
        logging.info(f'Total profit: {self.total_profit[Y]} {Y} and {self.total_profit[Z]} {Z}')
        logging.info(f'Number of transactions: {self.number_of_txs}')
        self.update_balance()
        self.balance_stat()
        logging.info('======================================================================================')

        return True

    def proceed(self, X, Y, Z):
        # logging.info(f'Let\'s find arbitrage opportunity on {X} - {Y} - {Z}')
        t = 1
        X_Y = X + '_' + Y
        Y_Z = Y + '_' + Z
        X_Z = X + '_' + Z
        X_Y_bid_order = self.get_bid_market_order(X_Y)
        Y_Z_bid_order = self.get_bid_market_order(Y_Z)
        X_Z_ask_order = self.get_ask_market_order(X_Z)

        if not X_Y_bid_order or not Y_Z_bid_order or not X_Z_ask_order:
            return

        self.bid_bid_ask(X, Y, Z, X_Y, Y_Z, X_Z, X_Y_bid_order, Y_Z_bid_order, X_Z_ask_order)

        X_Y_ask_order = self.get_ask_market_order(X_Y)
        Y_Z_ask_order = self.get_ask_market_order(Y_Z)
        X_Z_bid_order = self.get_bid_market_order(X_Z)

        if not X_Y_ask_order or not Y_Z_ask_order or not X_Z_bid_order:
            return

        self.ask_ask_bid(X, Y, Z, X_Y, Y_Z, X_Z, X_Y_ask_order, Y_Z_ask_order, X_Z_bid_order)

    def run(self):
        start_time = datetime.now().strftime('%d/%m/%y %H:%M:%S')

        t = 1

        while True:
            try:
                if msvcrt.kbhit() and ord(msvcrt.getch()) == 27:
                    break

                self.proceed('ETH', 'BTC', 'USDT')
                self.proceed('KNOW', 'ETH', 'BTC')
                self.proceed('KNOW', 'BTC', 'USDT')
                self.proceed('GTO', 'BTC', 'USDT')
                self.proceed('GTO', 'ETH', 'USDT')
                self.proceed('TRX', 'ETH', 'BTC')
                self.proceed('XPX', 'ETH', 'BTC')
                self.proceed('XEM', 'ETH', 'BTC')

                time.sleep(config.WAIT_FOR_NEW_ORDER)

                t = 1
            except Exception as e:
                msg = f'An unexpected error occurred. Error message: {e}'
                logging.info(msg)
                utils.send_telegram_message(msg)
                time.sleep(t)
                t = (t + 1) % config.MAX_WAIT_TIME

        # self.proceed('ETH', 'BTC', 'USDT')
        # self.proceed('KNOW', 'ETH', 'BTC')
        # self.proceed('KNOW', 'BTC', 'USDT')
        # self.proceed('GTO', 'KNOW', 'BTC')
        # self.proceed('GTO', 'KNOW', 'ETH')
        # self.proceed('GTO', 'BTC', 'USDT')

        curr_time = datetime.now().strftime('%d/%m/%y %H:%M:%S')

        logging.info('======================= TOTAL PROFIT ===========================')
        logging.info(f'\t(from {start_time} to {curr_time})')
        for coin in self.coins:
            logging.info(f'{self.total_profit[coin]} {coin}')
        self.balance_stat()
        logging.info('===========================================================')

if __name__ == '__main__':
    KTriArbBot().run()